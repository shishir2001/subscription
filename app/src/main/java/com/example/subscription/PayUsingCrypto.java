package com.example.subscription;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class PayUsingCrypto extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_using_crypto);
        RecyclerView recyclerView=findViewById(R.id.PayUsingCryptoActivityMonthlySubscriptionCardRecyclerview);
        int[] temp = new int[20];
        for(int i=0;i<7;i++)
        {
            temp[i]=i;
        }
        PaymentCryptoOptionAdapter2 adapter1 = new PaymentCryptoOptionAdapter2(this, temp);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter1);
    }
}