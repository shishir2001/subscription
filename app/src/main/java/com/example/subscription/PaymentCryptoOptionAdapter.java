package com.example.subscription;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PaymentCryptoOptionAdapter extends RecyclerView.Adapter<PaymentCryptoOptionAdapter.MyViewHolder>{

    Context context;
    int [] temp;

    public PaymentCryptoOptionAdapter(Context context, int[] temp) {
        this.context=context;
        this.temp=temp;
        Log.d("CHECKING", String.valueOf(temp.length));
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.item_crypto_option,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return temp.length;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView name;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
           imageView=itemView.findViewById(R.id.item_crypto_option_logo);
           name=itemView.findViewById(R.id.item_crypto_option_name);

        }
    }
}