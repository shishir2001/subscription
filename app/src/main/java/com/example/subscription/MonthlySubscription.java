package com.example.subscription;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class MonthlySubscription extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monthly_subscription);
        RecyclerView recyclerView2=findViewById(R.id.monthlySubscriptionCardRecyclerview);
        RecyclerView recyclerView1=findViewById(R.id.monthlySubscriptionCryptoRecyclerview);
        TextView viewAllCard=findViewById(R.id.monthlySubscriptionViewAllCard);
        TextView viewAllCrypto=findViewById(R.id.monthlySubscriptionViewAllCrypto);

        int[] temp = new int[20];
        for(int i=0;i<7;i++)
        {
            temp[i]=i;
        }
        PaymentCryptoOptionAdapter adapter1 = new PaymentCryptoOptionAdapter(this, temp);
        recyclerView1.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        recyclerView1.setAdapter(adapter1);
        PaymentCardOptionAdapter adapter2 = new PaymentCardOptionAdapter(this, temp);
        recyclerView2.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        recyclerView2.setAdapter(adapter2);
        viewAllCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), PayUsingCard.class);
                startActivity(intent);
            }
        });
        viewAllCrypto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), PayUsingCrypto.class);
                startActivity(intent);
            }
        });

    }
}