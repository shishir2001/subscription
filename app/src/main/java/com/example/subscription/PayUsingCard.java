package com.example.subscription;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class PayUsingCard extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_using_card);
        RecyclerView recyclerView1=findViewById(R.id.PayUsingCardActivityMonthlySubscriptionCardRecyclerview);
        int[] temp = new int[20];
        for(int i=0;i<7;i++)
        {
            temp[i]=i;
        }
        PaymentCardOptionAdapter adapter1 = new PaymentCardOptionAdapter(this, temp);
        recyclerView1.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        recyclerView1.setAdapter(adapter1);
    }
}